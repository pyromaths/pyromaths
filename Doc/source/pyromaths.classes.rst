Les classes de Pyromaths
========================

.. currentmodule:: pyromaths.classes

.. toctree::
    :maxdepth: 2

    pyromaths.classes.ensembles
    pyromaths.classes.Fractions
    pyromaths.classes.PolynomesCollege
    pyromaths.classes.SquareRoot
    pyromaths.classes.Vecteurs
    pyromaths.classes.premierdegre
