.. _pyromaths.classes.premierdegre:

:mod:`pyromaths.classes.premierdegre` --- Outils pour la résolution d'équations et inéquations du premier degré
===============================================================================================================

.. automodule:: pyromaths.classes.premierdegre
    :members:
