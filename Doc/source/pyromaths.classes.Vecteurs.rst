.. _pyromaths.classes.Vecteurs:

:mod:`pyromaths.classes.Vecteurs` --- Coordonnées et vecteurs
=============================================================

.. automodule:: pyromaths.classes.Vecteurs
    :members:
    :special-members: __abs__, __add__, __mul__, __neg__, __sub__
