.. _pyromaths.classes.ensembles:

:mod:`~pyromaths.classes.ensembles` --- Manipulation d'ensembles de nombres réels
=================================================================================

.. automodule:: pyromaths.classes.ensembles
   :members:
   :special-members: __contains__, __add__, __mul__
