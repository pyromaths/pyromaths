\exercice
\begin{enumerate}
\item Quel est le sens de variation de la fonction $f$ ? Répondre par une phrase en précisant les intervalles.
\item Tracer les tableaux de variation des fonctions $f$ et $g$.
\end{enumerate}
\begin{center}
\begin{tikzpicture}[very thick]
            \begin{axis}[axis lines=center,
        enlargelimits=true,
        axis line style={Maroon,-Stealth,line width=1.2pt},
        grid=both,
        minor tick num=1,
        minor grid style={draw=lightgray, line width=.4pt},
        major grid style={draw=Olive, line width=.6pt},
        xmin=-5,
        xmax=5,
        xtick distance=1,
        xlabel={$x$},
        ymin=-4,
        ymax=4,
        ytick distance=1,
        ylabel={$y$},
        ticklabel style={font=\tiny,fill=white},
        after end axis/.code={\path (axis cs:0,0) node [below left] {\tiny 0};},
]

\draw (axis cs:-5, 2) node[left]{$\mathcal{C}f$};
\draw[ultra thick, Maroon] (axis cs:-5, 2) .. controls (axis cs:-4.8, 2.2) and (axis cs:-4.2, 2.8) ..
(axis cs:-4, 3) .. controls (axis cs:-3.8, 3.2) and (axis cs:-3.2, 4.0) ..
(axis cs:-3, 4) .. controls (axis cs:-2.8, 4.0) and (axis cs:-2.2, 1.4) ..
(axis cs:-2, 1) .. controls (axis cs:-1.8, 0.6) and (axis cs:-1.2, 0.4) ..
(axis cs:-1, 0) .. controls (axis cs:-0.8, -0.4) and (axis cs:-0.2, -3.0) ..
(axis cs:0, -3) .. controls (axis cs:0.2, -3.0) and (axis cs:0.8, -1.3) ..
(axis cs:1, -1) .. controls (axis cs:1.2, -0.7) and (axis cs:1.8, 0.0) ..
(axis cs:2, 0) .. controls (axis cs:2.2, 0.0) and (axis cs:2.8, -4.0) ..
(axis cs:3, -4) .. controls (axis cs:3.2, -4.0) and (axis cs:3.8, -3.2) ..
(axis cs:4, -3) .. controls (axis cs:4.2, -2.8) and (axis cs:4.8, -2.2) ..
(axis cs:5, -2) 
;

\end{axis}
\end{tikzpicture}
\kern1cm
\begin{tikzpicture}[very thick]
            \begin{axis}[axis lines=center,
        enlargelimits=true,
        axis line style={Maroon,-Stealth,line width=1.2pt},
        grid=both,
        minor tick num=1,
        minor grid style={draw=lightgray, line width=.4pt},
        major grid style={draw=Olive, line width=.6pt},
        xmin=-5,
        xmax=5,
        xtick distance=1,
        xlabel={$x$},
        ymin=-4,
        ymax=4,
        ytick distance=1,
        ylabel={$y$},
        ticklabel style={font=\tiny,fill=white},
        after end axis/.code={\path (axis cs:0,0) node [below left] {\tiny 0};},
]

\draw (axis cs:-5, -1) node[left]{$\mathcal{C}g$};
\draw[ultra thick, Maroon] (axis cs:-5, -1) .. controls (axis cs:-4.8, -0.8) and (axis cs:-4.2, -0.30000000000000004) ..
(axis cs:-4, 0) .. controls (axis cs:-3.8, 0.30000000000000004) and (axis cs:-3.2, 1.6) ..
(axis cs:-3, 2) .. controls (axis cs:-2.8, 2.4) and (axis cs:-2.2, 4.0) ..
(axis cs:-2, 4) .. controls (axis cs:-1.8, 4.0) and (axis cs:-1.2, 4.0) ..
(axis cs:-1, 4) .. controls (axis cs:-0.8, 4.0) and (axis cs:-0.2, 4.0) ..
(axis cs:0, 4) .. controls (axis cs:0.2, 4.0) and (axis cs:0.8, 1.4) ..
(axis cs:1, 1) .. controls (axis cs:1.2, 0.6) and (axis cs:1.8, 0.5) ..
(axis cs:2, 0) .. controls (axis cs:2.2, -0.5) and (axis cs:2.8, -4.0) ..
(axis cs:3, -4) .. controls (axis cs:3.2, -4.0) and (axis cs:3.8, -0.5) ..
(axis cs:4, 0) .. controls (axis cs:4.2, 0.5) and (axis cs:4.8, 0.8) ..
(axis cs:5, 1) 
;

\end{axis}
\end{tikzpicture}
\end{center}